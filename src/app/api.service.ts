import { Injectable, Inject } from '@angular/core';
import { Request, RequestOptions, Http, Headers, RequestMethod } from '@angular/http';
import 'rxjs/Rx';
import { Summoner } from './shared/models/summoner';

@Injectable()
export class ApiService {
  private apiKey = "RGAPI-ca7d77d5-0be7-4538-abeb-dc91a3806586";
  private apiUrl = "https://na.api.pvp.net";
  private headers = new Headers({
    "Content-Type": "application/json",
    "Accept": "application/json"
  })
  public summoner: Summoner;


  constructor(private http: Http) {}

  public GetSummonerByName(name: string) {
    return this.http.get(this.apiUrl + '/api/lol/NA/v1.4/summoner/by-name/' + name + '?api_key=' + this.apiKey)
      .map(res => res.json())
  }

  public GetMatchHistory(SumId: number){
    return this.http.get(this.apiUrl + '/api/lol/na/v2.2/matchlist/by-summoner/' + SumId + '?api_key=' + this.apiKey)
    .map(res => res.json())
  }

  public GetMatchDetails(MatchId: number){
    return this.http.get(this.apiUrl + '/api/lol/na/v2.2/match/' + MatchId + '?api_key=' + this.apiKey)
    .map(res => res.json())
  }

  public GetChampions(){
    return this.http.get('http://ddragon.leagueoflegends.com/cdn/7.5.2/data/en_US/champion.json')
    .map(res => res.json())
  }
}
