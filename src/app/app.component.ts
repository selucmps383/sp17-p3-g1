import { Component } from '@angular/core';
import { ApiService } from './api.service';
import { Summoner } from './shared/models/summoner';
import { Match } from './shared/models/match';
import { MatchDetail } from './shared/models/matchdetail';
import { Champion } from './shared/models/champion';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public SummonerName : string;
  public summ: Summoner = new Summoner();
  public matches: Match[] = new Array<Match>();
  public matchDetails: MatchDetail[] = new Array<MatchDetail>();
  public champList: Champion[] = new Array<Champion>();

  constructor(private apiService: ApiService) {
    this.apiService.GetChampions()
      .subscribe(objects => {
        let temp = objects["data"];
        for(let champ in temp){
          this.champList.push(new Champion(temp[champ]))
        }
      })
  }

  onSearchClick(summonerName : string){
    console.log("Searching for " + summonerName.split(",")[0]);
    this.apiService.GetSummonerByName(summonerName.split(",")[0])
      .subscribe(objects => {
        this.summ = <Summoner>objects[summonerName.split(",")[0]];

        this.apiService.GetMatchHistory(this.summ.id)
          .subscribe(object => {
            //this.matches = <Match[]>object['matches'];
            let temp = object['matches'].slice(0,4);
            let x = 0;
            for(x = 0; x<4; x++){
              let currmatch = new Match(temp[x]);
              let f = 0;
              let game: MatchDetail;
              let win: string;
              console.log(currmatch)
              this.apiService.GetMatchDetails(currmatch.matchId)
                .subscribe(detail => {
                  game = new MatchDetail(detail);
                  game.champion = currmatch.champion;
                  this.matchDetails.push(game);

                  console.log(game);
                  for(f = 0; f<10; f++){
                    if(game.participantIdentities[f].player.summonerId == this.summ.id){
                      for(let j = 0; j<10; j++){
                        if(game.participants[j].participantId == game.participantIdentities[f].participantId){
                          console.log(game.participants[j].stats.winner ? 'Victory' : 'Defeat')
                          game.winner = game.participants[j].stats.winner ? 'Victory' : 'Defeat';
                          game.part = j;
                          console.log(game.part)
                        }
                      }
                    }
                  }
                });
              
              this.matches.push(currmatch);
            }
            
          });
      },
      (err=>{
        console.log(err);
      }));
  }
    
    latestTen() : Array<MatchDetail>{
        return this.matchDetails;
    }
    getChampImg(m : MatchDetail) : string{
        let champ = this.champList.filter(x => x.champkey == '' + m.champion)
        return 'http://ddragon.leagueoflegends.com/cdn/7.5.2/img/champion/' + champ[0].id + '.png';
    }
    getItemImg(i : number) : string{
      return 'http://ddragon.leagueoflegends.com/cdn/7.5.2/img/item/' + i + '.png';
    }
    getMatchWin(m: number){
          
            //return 'failure';
          

    }
}
