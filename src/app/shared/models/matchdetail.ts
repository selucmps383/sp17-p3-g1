export class MatchDetail{
    champion: number;
    region: string;
    matchtype: string;
    participants: any;
    season: string;
    matchDuration: number;
    matchId: number = 0;
    participantIdentities: Array<any>;
    winner: string;
    part: number;

    constructor(detail: any){
        this.participants = detail.participants;
        this.participantIdentities = detail.participantIdentities;
        this.matchId = detail.matchId;
    }
}