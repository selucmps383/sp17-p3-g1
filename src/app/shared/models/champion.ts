export class Champion{
    public id: string;
    public champkey: string;
    
    constructor(champ: any){
        this.id = champ.id;
        this.champkey = champ.key;

    }
}