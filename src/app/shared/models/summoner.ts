export class Summoner {
    id: number;
    name: string;
    profileIconId: number;
    revisionDate: number;
    summonerLevel: number;

    static fromObject(id: number, name: string, profileIconId: number, revisionDate: number, summonerLevel: number){
        var obj = new Summoner();
        obj.id = id;
        obj.name = name;
        obj.profileIconId = profileIconId;
        obj.revisionDate = revisionDate;
        obj.summonerLevel = summonerLevel;
        return obj;
    }
}