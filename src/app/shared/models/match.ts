export class Match{
    timestamp: number;
    champion: number;
    region: string;
    queue: string;
    season: string;
    matchId: number;
    role: string;
    platformId: string;
    lane: string;
    winner: string;

    constructor(match: any){
        this.timestamp = match.timestamp;
        this.champion = match.champion;
        this.region = match.region;
        this.queue = match.queue;
        this.season = match.season;
        this.matchId = match.matchId;
        this.role = match.role;
        this.platformId = match.platformId;
        this.lane = match.lane;
    }

    static fromObject(timestamp: number, champion: number, region: string, queue: string, season: string,
                        matchId: number, role: string, platformId: string, lane: string){
        var obj = new Match();
        obj.timestamp = timestamp;
        obj.champion = champion;
        obj.region = region;
        obj.queue = queue;
        obj.season = season;
        obj.matchId = matchId;
        obj.role = role;
        obj.platformId = platformId;
        obj.lane = lane;
        return obj;
    }


}